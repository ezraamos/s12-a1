console.log("This is another JS script")

//Syntax and Statements

//Syntax  set of rules that describes how statements must be constructed

//Statement instructions that we tell the computer to perform
	//usually ended with semi-colon

//variables
 	//It is used to contain data
 	//named location for stored value
 	 	//1 word
 	 	//starts with an alphabet
 	 	//1st word should be lowercase
 	 	//if 2 words, the second word initial word will be in upper case
 	 		//firstName
 	 		//favorteFruit
 	 	//camel case
 	 	    //camelHumpHumpagain
 	// v -old version

 	//should be descriptive

 	//let numberOfWorkingDays - 0;
 	//let currentBalance = 100;


    //let <variableName> = <variableValue>


    let productName = 'Desktop Computer';
    let productPrice = 18999;
    const pi = 3.1416


    let productCode = 'DC073', productBrand = 'Dell'


    console.log(productName);
    console.log(productPrice);
    console.log(pi);
    console.log(productCode, productBrand);

    console.warn('You have been warned!');
    console.error('Capstone is coming!');

    let jowa='wala'
    console.log(jowa);

    //let let='test'
    //console.log(let)

//Data Types
	//String
	let country = 'Philippines';
	let province = "Metro Manila";

	let greetings = "It's raining today"
	let mailAddress = 'Metro Manila\n\nPhilippines'
	



	//let fulAdress = province + ',' + country


	let fullAddress = `Hello ${country}`;

	console.log(fullAddress);


	//Numbers

	let grade = 98.99;
	let headCount = 14;
	let planetDistance = 2e10;
	
	let formula = 2^10;

	console.log(formula);

	//Boolean

	let isMarried = false;
	let isGoodConduct = true;

	//Array

	let grades = [75,77,78,79];
	let friends = ['Ben','Mr. Tan','Jana'];

	//zero indexing

	//console.log(friends[0]);

	//Object
		// - can store multiple values and data
		//  we want our arrays to contain a uniform type of data.
		// - is instantiatedorcreated using curly
		// - contains keys and values
		// -key is like your variable name, it is the property of the object
		// paired with the key, is its value. 

	let person1 = {

		firstName: "Michael",
		lastName: "Jordan",
		championships: 6,
		isMVP: true,
	}
  	
  	console.log(person1.championships)
  	console.log(person1.isMVP)

  	//Mathematical Operators

  		//Addition

  		let ex1 = 15;
  		let ex2 = 5
  		console.log(ex1+ex2)//integer 20

  		let exNum = "15"
  		let exNum2 = "5"
  		console.log(exNum + exNum2)
  		//note:use integers or make sure that numbers being involved in a mathematical operation are number types not string.
  		let sum1 = ex1 + ex2
  		console.log(sum1)

  		//Subtraction
  		 console.log(ex1-ex2)//10

  		//Division
  		console.log(ex1/ex2)//3

  		//Multiplication
  		console.log(ex1*ex2)//75

  		//Modulo
  		console.log(ex1 % ex2)//15/5=3 remainder 0

  		//MDAS & PMDAS

  		 let pmdas =(3+4)*5
  		 console.log(pmdas)//35

  		 //Assignment Operators

  		 //Basic Assignment operator(=)
  		 //Basic Assignment operator lets us store the value on the right to the variable on the left
  		  let number =5

  		  //undefined and null data types

  		  let person 
  		  //A variable has been declaredd but there is no assignment or initialization.
  		  //Initialization is the assignment of an initial value.
  		  console.log(person)//undefined
  		  //undefined value is the result of a variable being declared but no initialization

  		  //null
  		  let nothing = null
  		  //null -intentional absence of value
  		  let numberZ = 0

  		  //Additional Assignment Operator
  		  //we can use addition assignment operator if we want to add the value to right of the basic assignment operator to the variable ont the left
  		   let price1=50
  		   let price2=25

  		    price1 += price2

  		    console.log(price1)//75

  		    //Subtraction Assignment Operator

  		    // because price 1 is now 75
  		    price1 -=price2
  		    console.log(price1)//50
  		    console.log(price2)


  		    //Multiplication Assignment Operator
  		    price1 *= price2
  		    console.log(price1)//50*25

  		    //Division Assignment Operator
  		    price1 /= price2 
  		    console.log(price1)// 1250/25


  		    //Increment and Decrement
  		    //Increment adds 1 to the value of the variable.(++)
  		    //Decrement subtracts 1 to the value of th evariable(--)

  		    //price 1 is now 50

  		    ++price1
  		    console.log(price1)//51

  		    ++price1 
  		    console.log(price1)//52

  		    --price1
  		    console.log(price1)//51

  		    //Prefix increment/decrement
  		    console.log(price1++)
  		    console.log(price1)
  		    //prefix increment increments the number first and returns the incremented number
  		    //postfix increment increments the number but returns the original value first


  		    //Type Coercion
  		    //Transformation of one data type to another
  		    //Javascript  does some forced type coercions in its operations
  		    //parseInt()- will convert a data to an integer as a whole number

  		    let age = "15"
  		    console.log(parseInt(age))



  		    //parseFloat() for decimal number.
  		    //toFixed() sets a number to the indicated decimal places

  		    let coercion = 12.12 +12
  		    console.log(coercion.toFixed (4))//24.1200 but as a string
  		    console.log(parseInt(coercion))
  		    console.log(parseFloat(coercion))

  		    //forced typed coercion

  		    let stringtoNum = "30"
  		    let stringtoNum2 = "15"
  		    console.log(stringtoNum-stringtoNum2)//15-integer
  		    //Javascript forced our strings into number
  		    console.log(stringtoNum+stringtoNum2)
  		    //3015 -concatenate
  		    //Concatenate -is the forced combination of strings

  		    //Comparison Operators

  		    //Equality Operator
  		    //It compares the values of the variable on the left and the right of the operator (==).the result of the comparison is boolean

  		    let juan = "juan"
  		    let ageOfPerson =25

  		    console.log(25==25)//true
  		    console.log(25==24)//false
  		    console.log("25" == ageOfPerson)//true (when using == Equality operator, Javascript performs forced coercion)

  		    console.log(juan == "25")//false- although the data type is the same, the values are different



  		    //Inequality Operator
  		    //It compares the values of the variable on the left and the right of the operator (!=) and seeks inequalityfrom both compared values. The operators will result to true if the values are not equal or the same.

  		    console.log(25 != 24)//true

  		    console.log('juan' != juan)//false

  		    console.log(2 != false)//true
  		    //is 2 not equal to false? yes
  		    // false=0 true=1


  	//Strict Equality  Operator
  	//Strict Equality Operator seeks equality between two variables BUT does not perform type coercion

  	 		let pedro ='pedro'
  	 		let ageOfPedro = 20
  	 		console.log(20 === 20)//true note: its the same data type and value
  	 		console.log('20' === ageOfPedro)//false
  	 		//is 20 strictly equal to 20?no.
  	 		//Strict Equality not only compares the values but also the types

  	 		//Strict Inequality
  	 		//Strict Inequality Operator seeks inequality between two variables/values
  	 		//It does not perform type coercion

  	 		console.log(20 !== 21)//true


  	 		console.log(20 !== "21")//true

  	 		console.log(1 !== "1")//true


  	 		//Relational Operators 

  	 		// (>) (<) (>=) (<=)

  	 		let jordan = 6
  	 		let lebron = 4

  	 		console.log(jordan > 4)//true
  	 		console.log(lebron>jordan)//false

  	 		let redTeam=50
  	 		let blueTeam=70

  	 		console.log(redTeam <blueTeam)//true

  	 		let greenTeam =70
  	 		
  	 		console.log(redTeam>=greenTeam)//false

  	 		console.log(blueTeam>=greenTeam)//true

  	 		let yellowTeam=50
  	 		 console.log(greenTeam <= yellowTeam)
  	 		 console.log(yellowTeam <= blueTeam)

  	 		 //logical operators
  	 		 //-are used to determine the logic between variables and values
  	 		 //When used, it also return a boolean

  	 		 //&& and
  	 		 //When using and logical operator, if the values on both left side and right side of the operator is equal to true, then && would resullt to a value of true

  	 		 let isLegalAge = true;
  	 		 let isRegistered = false;

  	 		 console.log(isLegalAge && isRegistered)//false because the value of the left suude of the operator is true but the right side of the operator is false
  	 		 //its like asking if both requirements were met. If not, then we return false

  	 		 //redTeam=50 blueTeam=70 greenTeam=70 yellowTeam=50

  	 		 console.log((redTeam >= yellowTeam) && (greenTeam >= blueTeam))//true
  	 		 console.log((redTeam >= blueTeam) && (greenTeam >= yellowTeam))//false

  	 		 // ||(or) logical operator
  	 		 //When using || (or) logicaloperator, the statement will result to true if the value on either theleft or right is true

  	 		 console.log(isLegalAge || isRegistered)//true

  	 		 // ! (not operator)
  	 		 // ! when attached to a variable, we ask if the variable is NOT true
  	 		 console.log(!isLegalAge)//false
  	 		 console.log(!isRegistered)//true

  	 		 console.log(!(redTeam>=blueTeam))//true

  	 		 //&& and operator- true if both requirements are met or true
  	 		 //|| or operator-true if at least 1 requirement is true
  	 		 //! operator asks if thevalue is not equal to true, if it is false will be returned



  	 		 //functions 
  	 		 //- is a block of code which can be run or used repeatedly
  	 		 //To create a function first use the function keyword:
  	 		 //add the name of your function
  	 		 //parenthesis will receive data or arguments to be passed and used into our function
  	 		 //{} is the code block of our function, this is where we put the statements to run

  	 		 function hello(){
  	 		 	//Alert a method which shows an alert on the browser.
  	 		 	alert("hi, Batch73")



  	 		 }

  	 		 //invoke/call the function to use it.


  	 		 //To receive an argument, we must provide a parameter

  	 		 function myName(name){

  	 		 	//parameter will act as a variable which represents the data passed an argument.
  	 		 	alert(name)
  	 		 }

  	 		// myName("Ezra Amos")//argument -data to be passed into a function
  	 		 //note: the data were providing as an argument is expected to be a name
  	 		 //So, that it's(code) is properly  read
  	 		 // The parameter of your function is preferably descriptive of the argument

  	 		 function showFullName(firstName,middleName,lastName){

  	 		 	//+ to concatenate/combine strings
  	 		 	// Javascript space is considered a character in a string

  	 		 	alert(firstName + " " + middleName + " " + lastName)
  	 		 }

  	 		 //use/call our function:
  	 		 showFullName("Ezra","Amos","Mollena")

  	 		 //activity
  	 		 //create a fuction which receives two arguments and produces the /displays its sum in the console
  	 		 //create a function which receives two arguments and displays its product in th console

  	 		 function sum(firstNumber,secondNumber){
  	 		 	alert(firstNumber+secondNumber)

  	 		 }
			 function product(firstNumber,secondNumber){
			  	 		 	alert(firstNumber*secondNumber)
			  	 		 }

			 


